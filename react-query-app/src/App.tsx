import React, { useState } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import WeatherSearch from './components/WeatherSearch';
import WeatherDisplay from './components/WeatherDisplay';
import WeatherCitylist from './components/WeatherCitylist';
import WeatherProperties from './components/WeatherProperties';
import WeatherForecast from './components/WeatherForecastMinutely';

const queryClient = new QueryClient();

function App(): JSX.Element {
  const [location, setLocation] = useState<string>('');

  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <h1>Weather Dashboard</h1>
        <WeatherSearch setLocation={setLocation} />
        <WeatherDisplay location={location} />
        <WeatherProperties location={location}/>
        <WeatherForecast location={location}/>
        <WeatherCitylist setLocation={setLocation}/>
      </div>
    </QueryClientProvider>
  );
}

export default App;
