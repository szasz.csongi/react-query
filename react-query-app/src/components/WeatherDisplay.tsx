import React, { useState } from 'react';
import { useQuery, useMutation } from 'react-query';
import axios from 'axios';

interface WeatherDisplayProps {
  location: string;
}

const fetchWeatherData = async (location: string) => {
  const response = await axios.get(
    `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=cf6bb5ac17f2c30360bb1a63fdf14135&units=metric`
  );
  return response.data;
};

function WeatherDisplay({ location }: WeatherDisplayProps): JSX.Element {
  const { data, isLoading, isError: queryIsError } = useQuery(
    ['weather', location],
    () => fetchWeatherData(location),
    { enabled: !!location }
  );

  const { mutate, isError: mutationIsError, isSuccess } = useMutation<void, any, any>(
    (weatherDataToSend) =>
      fetch('http://localhost:5000/save/newWeather', {
        method: 'POST',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(weatherDataToSend),
      }).then((res) => res.json())
  );

  const [isSaveSuccess, setIsSaveSuccess] = useState<boolean>(false);

  if (isLoading) return <div className='component-div component-border'>Loading...</div>;
  if (queryIsError) return <div className='component-div component-border'>Error fetching data</div>;


  const handleClick = () => {
    const weatherDataToSend = {
      name: data.name,
      temperature: data.main.temp,
      humidity: data.main.humidity,
      description: data.weather[0].description,
    };
    mutate(weatherDataToSend, {
      onSuccess: () => {
        setIsSaveSuccess(true);
        setTimeout(() => {
          setIsSaveSuccess(false);
        }, 4000);
      },
    });
  };

  return (
    <div>
      {data && (
        <div className='component-div component-border'>
          <h2>{data.name}</h2>
          <p>Temperature: {data.main.temp} °C</p>
          <p>Humidity: {data.main.humidity}%</p>
          <p>Weather: {data.weather[0].description}</p>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <button onClick={handleClick}>Save City</button>
            {isSaveSuccess && <span style={{ color: 'green', marginLeft: '10px' }}>City saved successfully!</span>}
          </div>
        </div>
      )}
    </div>
  );
}

export default WeatherDisplay;
