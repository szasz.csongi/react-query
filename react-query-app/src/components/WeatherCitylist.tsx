import React, { useState } from 'react';
import { useQuery } from 'react-query';

interface WeatherCitylistProps {
  setLocation: React.Dispatch<React.SetStateAction<string>>;
}

const fetchWeatherCitylistData = async () => {
  const response = await fetch('http://localhost:5000/get/getWeather');
    if (!response.ok) {
      throw new Error('Failed to fetch saved cities');
    }
    return response.json();
};

function WeatherCitylist({ setLocation }: WeatherCitylistProps): JSX.Element {
  const { data, isLoading, isError } = useQuery<any[]>('savedCities', fetchWeatherCitylistData, {
    refetchInterval: 2000,
  });

  if (isLoading) {
    return <div className='component-div component-border'>Loading cities...</div>;
  }

  if (isError) {
    return <div className='component-div component-border'>Error fetching cities</div>;
  }

  const handleSelectedCity = (cityName: string) => {
    setLocation(cityName);
  };

  console.log(data);

  return (
    <div>
      {data && data.length !== 0 && (
        <div className='component-div component-border'>
          <h2>Saved Cities</h2>
          <ul>
            {data.map((item, index) => (
              <li key={index}>
                {item.name}
                <button onClick={() => handleSelectedCity(item.name)}>Show city</button>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}

export default WeatherCitylist;
