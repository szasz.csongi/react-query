import React, { useState } from 'react';

interface WeatherSearchProps {
  setLocation: React.Dispatch<React.SetStateAction<string>>;
}

function WeatherSearch({ setLocation }: WeatherSearchProps): JSX.Element {
  const [searchTerm, setSearchTerm] = useState<string>('');

  const handleSearch = () => {
    setLocation(searchTerm);
  };

  return (
    <div className="component-div">
      <input
        type="text"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        onKeyDown={e => e.key === 'Enter' ? handleSearch() : ''}
      />
      <button onClick={handleSearch}>Search</button>
    </div>
  );
}

export default WeatherSearch;
