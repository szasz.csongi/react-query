import { useQuery } from "react-query";
import axios from "axios";

interface WeatherDisplayProps {
  location: string;
}

const fetchWind = async (location: string) => {
  const response = await axios.get(
    `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=cf6bb5ac17f2c30360bb1a63fdf14135&units=metric`
  );
  return response.data;
};
function WeatherProperties({ location }: WeatherDisplayProps): JSX.Element {
  const { data, isLoading, isError } = useQuery(
    ["wind", location],
    () => fetchWind(location),
    {
      enabled: !!location,
      refetchInterval: 1000 * 60 * 10,
      staleTime: 1000 * 60 * 10,
      cacheTime: 1000 * 60 * 15,
    }
  );
  if (isLoading) return <div className='component-div component-border'>Loading detailed weather...</div>;
  if (isError) return <div className='component-div component-border'>Error fetching detailed weather</div>;
  return (
    <div>
      {data && (
        <div className='component-div component-border'>
          <h4>Additional info</h4>
          <p>Last update: {new Date(data.dt * 1000).toLocaleTimeString("ro-RO")}</p>
          <p>Wind: {data.wind.speed} m/s</p>
          <p>Real feel: {data.main.feels_like} °C</p>
          <p> Sunrise: {new Date(data.sys.sunrise * 1000).toLocaleTimeString("ro-RO")}</p>
          <p> Sunset: {new Date(data.sys.sunset * 1000).toLocaleTimeString("ro-RO")}</p>
        </div>
      )}
    </div>
  );
}
export default WeatherProperties;
