import { useQuery } from "react-query";
import axios from "axios";
import { Key } from "react";

interface WeatherDisplayProps {
  location: string;
}

const fetchForecast = async (location: string) => {
  const response = await axios.get(
    `https://api.weatherbit.io/v2.0/forecast/minutely?city=${location}&key=9fa1e765d9364758bad9823b925f1a40`
  );
  return response.data;
};
function WeatherForecast({ location }: WeatherDisplayProps): JSX.Element {
  const { data, isLoading, isError } = useQuery(
    ["forecast", location],
    () => fetchForecast(location),
    {
      enabled: !!location,
      refetchInterval: 60000,
      refetchOnWindowFocus: true,
    }
  );
  if (isLoading) return <div className='component-div component-border'>Loading forecast...</div>;
  if (isError) return <div className='component-div component-border'>Error fetching forecast</div>;
  if (!data) return <div></div>;
  const tenMinForecast = data.data.slice(0, 10);
  return (
    <div className='component-div component-border'>
      <p>10 minute forecast:</p>
      <ul>
        {tenMinForecast.map(
          (
            item: {
              timestamp_local: string;
              temp: number;
            },
            index: Key | null | undefined
          ) => (
            <li key={index}>
              {item.timestamp_local.split('T')[1]}: {item.temp} °C
            </li>
          )
        )}
      </ul>
    </div>
  );
}
export default WeatherForecast;
